resource "aws_key_pair" "keypair" {
    key_name = "examplekey"
    public_key = file("~/.ssh/id_rsa.pub")
}

resource "aws_instance" "example" {
    key_name = aws_key_pair.keypair.key_name
    ami = "ami-09246ddb00c7c4fef"
    instance_type = "t2.micro"
    tags = {
      Name = "MyFifthTerraformInstance"
      #Name = var.instance_name
    }
    provisioner "local-exec" {
        command = "echo ${aws_instance.example.public_ip} > ip_adress.txt"
    }

    connection {
        type = "ssh"
        user = "ec2-user"
        private_key = file("~/.ssh/id_rsa")
        host = self.public_ip
    }

    provisioner "remote-exec" {
        inline = [
            "sudo yum -y update",
            "sudo yum -y install httpd",
            "sudo systemctl enable httpd",
            "sudo systemctl start httpd"
        ]
    }
}